from bs4 import BeautifulSoup
import requests
import os
import re
import pickle
from multiprocessing import Process

#la fonction f1 concerne le recensement de toutes les addresses d'annonces  dans la partie emploi non salarie du site
def f1():

    # ces deux lignes de code permettent de se connecter au site en passant par
    # le proxy d'Alten
    os.environ['http_proxy'] = "http://172.17.4.161 :8000"
    os.environ['https_proxy'] = "https://172.17.4.161 :8000"

    list_offer_url_nonsalarie = []

    print('executing_nonsalarie')

    l = 1
    #La boucle while ci-dessous indique qu'on continue a boucler sur toutes les pages du site dans la categorie emploi non salarie tant que chaque page affiche des annonces
    keep_going_nonsalarie = True
    
    while keep_going_nonsalarie:
        keep_going_nonsalarie = False
        
        url_nonsalarie = "https://www.1taf.com/annonces/pour-trouver-un-emploi/offres/non-salarie?&page=" + str(l)

        page_nonsalarie = requests.get(url_nonsalarie)
        data_nonsalarie = page_nonsalarie.text
        soup_nonsalarie = BeautifulSoup(data_nonsalarie, 'html.parser')
        
        #toutes les annonces commencent par "https://www.1taf.com/annonce/" donc extrait tous les liens vers les annonces de chaque page à l'aide de la ligne de code ci-dessous.
        find_result_nonsalarie = soup_nonsalarie.find_all(attrs={'href': re.compile("https://www.1taf.com/annonce/")})
        
        #Cette boucle démarrera seulement si la variable find_result_nonsalarie n'est pas vide e.g. si la page choisie comporte des annonces
        for link_nonsalarie in find_result_nonsalarie:
            
            list_offer_url_nonsalarie.append(link_nonsalarie.get('href'))
            l += 1
            keep_going_nonsalarie = True


    print(l)
    #On stocke les adresses de chaque annonce dans un fichier 
    with open('address_nonsalarie', 'wb') as fp:
        pickle.dump(list_offer_url_nonsalarie, fp)
    
    #On cree une liste vide qui contiendra les numeros de chaque annonce
    if not os.path.exists('numero_nonsalarie'):
        a_nonsalarie = []
        with open('numero_nonsalarie', 'wb') as yolo:
            pickle.dump(a_nonsalarie, yolo)
    
    #On execute le fichier scraping-emploi_nonsalarie qui recuperera le texte de chaque annonce
    os.system("py scraping-emploi_nonsalarie.py")

    # Linux version
    # os.system("python3 scraping-emploi_salarie.py")

#la fonction f2 est construite sur le meme modele que la fonction f1, elle traitera les offres des emplois salaries
def f2():

    # os.environ['http_proxy'] = "http://172.17.4.161 :8000"
    # os.environ['https_proxy'] = "https://172.17.4.161 :8000"

    list_offer_url = []

    print('executing')

    j = 1
    #
    keep_going = True
    
    while keep_going:
        keep_going = False
    #for i in range(1, 3):
        # for i in range(1)
        url = "https://www.1taf.com/annonces/pour-trouver-un-emploi/offres/salarie?&page=" + str(j)

        page = requests.get(url)
        data = page.text
        soup = BeautifulSoup(data, 'html.parser')
        
        find_result = soup.find_all(attrs={'href': re.compile("https://www.1taf.com/annonce/")})
        
        for link in find_result:
            
            list_offer_url.append(link.get('href'))
            j += 1
            keep_going = True


    print(j)
    
    with open('address_salarie', 'wb') as fp:
        pickle.dump(list_offer_url, fp)

    if not os.path.exists('numero_salarie'):
        a = []
        with open('numero_salarie', 'wb') as yolo:
            pickle.dump(a, yolo)

    #Linux version
    #os.system("python3 scraping-emploi_salarie.py")

    #Windows version
    os.system("py scraping-emploi_salarie.py")


#Ce morceau de code permet de paralléliser le code.
if __name__=='__main__':
    p1 = Process(target = f1)
    p1.start()
    
    p2 = Process(target = f2)
    p2.start()
    
    p1.join()
    p2.join()
