# scraping
1 - On exécute creation-user-database.py pour créer une database et un
utilisateur.

2 - On exécute scraping.py qui exécutera tout seul les codes scraping-emploi_nonsalarie.py et scraping-emploi_salarie.py

3 - Les données textuelles seront stockées dans une database nommée untaf_dev via l'utilisateur nommé untaf_dev

4 - En fonction de l'OS utilisé, il faudra changer les lignes qui exécutent les fichier scraping-emploi_nonsalarie.py et scraping-emploi_salarie.py (respectivement lignes 59 et 105 du fichier scraping.py).
Par défaut, l'exécution est écrite sous Windows

5 - Les cv provenant du site ne sont pas gérés dans ce code par manque d'accès à cette partie du site.
