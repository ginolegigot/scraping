# -*- coding: utf-8 -*-
import urllib
from urllib import request
import os
from datetime import datetime
from bs4 import BeautifulSoup
import unidecode
from pymongo import MongoClient
import pickle
import sys

#ce code extrait le texte de chaque annonce des emplois non salaries, le code convernant le scraping des emplois slaraies est construit de la meme maniere

try:
    os.environ['http_proxy']= "http://172.17.4.161 :8000"
    os.environ['https_proxy']= "https://172.17.4.161 :8000"

    print("working_nonsalarie")

    with open ('address_nonsalarie', 'rb') as gp:
        offer_addresses_nonsalarie = pickle.load(gp)

    with open ('numero_nonsalarie', 'rb') as hp:
        numero_nonsalarie = pickle.load(hp)




    # headers
    headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.70 Safari/537.36'}

    # print(offer_addresses[0][-7:])

    numerodannonceslist_nonsalarie=[]

    for k in range(len(offer_addresses_nonsalarie)):

        if offer_addresses_nonsalarie[k][-7:] not in numero_nonsalarie:

            company_page_nonsalarie = '<'+offer_addresses_nonsalarie[k]+'>'
            numerodannonceslist_nonsalarie.append(offer_addresses_nonsalarie[k][-7:])

            # opening a desired page
            page_request_nonsalarie = request.Request(company_page_nonsalarie, headers=headers)
            page_nonsalarie = request.urlopen(page_request_nonsalarie)

            # parse the html using beautifulsoup
            html_content_nonsalarie = BeautifulSoup(page_nonsalarie, 'html.parser')

            #aiming the text paragraph from each offers
            body_nonsalarie = html_content_nonsalarie.find('div', attrs={'class': 'panel-body'})
            body_nonsalarie = body_nonsalarie.get_text(' ', strip=True)
            # print(body)

            text_nonsalarie = unidecode.unidecode(body_nonsalarie)
            # print(text)
            
            #extracting date
            date_nonsalarie = html_content_nonsalarie.find('span', attrs={'itemprop': 'datePosted'})
            date_nonsalarie = date_nonsalarie.get_text(' ', strip=True)
            date_nonsalarie = datetime.strptime(date_nonsalarie, '%d/%m/%Y').strftime('%Y-%m-%d %H:%M:%S')

            #Storing in db

            client = MongoClient(port=27017, username="untaf_dev", password="password", authSource="untaf_dev")
            db = client.untaf_dev

            data_nonsalarie = {
                "type": "job",
                "source": "1taf.com",
                "language": "fr",
                "date": date_nonsalarie,
                "txt": text_nonsalarie,
                "format" : "txt",
                "encoding" : "utf-8",
                "mime" : "text/plain",
                "type d'emplois": "nonsalarie"
            }
            # Step 3: Insert data in document collection

            result = db.document.insert_one(data_nonsalarie)
            numero_nonsalarie.append(offer_addresses_nonsalarie[k][-7:])

    #On stocke le numero des annonces traitees afin de ne pas les retraiter si on relance le code
    with open('numero_nonsalarie', 'wb') as yolo:
        pickle.dump(numero_nonsalarie, yolo)


#On enregistre les messages d'erreur dans un fichier de logs
except urllib.error.URLError:
    print(sys.exc_info()[0],":",sys.exc_info()[1])
    with open("error.log", "a") as tx:
        print(f"{datetime.now():'%Y-%m-%d %H:%M:%S'} source: proxy {sys.exc_info()[0]} : {unidecode.unidecode(str(sys.exc_info()[1]))} Solution: ajouter les lignes de codes suivantes: os.environ['http_proxy']= 'http://172.17.4.161 :8000'"
              f" et 'os.environ['https_proxy']= 'https://172.17.4.161 :8000'", file=tx)
    sys.exit(1)

except urllib.error.HTTPError:
    print(sys.exc_info()[0], ":", sys.exc_info()[1])
    with open("error.log", "a") as tx:
        print(f"{datetime.now()} source: proxy {sys.exc_info()[0]} : {unidecode.unidecode(str(sys.exc_info()[1]))} Solution: liste exhaustive des différents types d'erreur HTTP: https://python.readthedocs.io/en/stable/howto/urllib2.html#httperror", file=tx)
    sys.exit(2)

except:
    print(sys.exc_info()[0], ":", sys.exc_info()[1]),
    with open("error.log", "a") as tx:
        print(f"{datetime.now()} source: unknown {sys.exc_info()[0]} : {unidecode.unidecode(str(sys.exc_info()[1]))} Solution: unknown ",file=tx)
    sys.exit(3)













