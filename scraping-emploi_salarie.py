# -*- coding: utf-8 -*-
import urllib
from urllib import request
import os
from datetime import datetime
from bs4 import BeautifulSoup
import unidecode
from pymongo import MongoClient
import pickle
import sys


try:
    os.environ['http_proxy']= "http://172.17.4.161 :8000"
    os.environ['https_proxy']= "https://172.17.4.161 :8000"

    print("working")

    with open ('address_salarie', 'rb') as fp:
        offer_addresses = pickle.load(fp)

    with open ('numero_salarie', 'rb') as wp:
        numero = pickle.load(wp)


    # headers
    headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.70 Safari/537.36'}

    # print(offer_addresses[0][-7:])

    numerodannonceslist=[]

    for i in range(len(offer_addresses)):

        if offer_addresses[i][-7:] not in numero and i <=10:

            company_page = '<'+offer_addresses[i]+'>'
            numerodannonceslist.append(offer_addresses[i][-7:])

            # opening a desired page
            page_request = request.Request(company_page, headers=headers)
            page = request.urlopen(page_request)

            # parse the html using beautifulsoup
            html_content = BeautifulSoup(page, 'html.parser')

            #aiming the text paragraph from each offers
            body = html_content.find('div', attrs={'class': 'panel-body'})
            body = body.get_text(' ', strip=True)
            # print(body)

            text = unidecode.unidecode(body)
            # print(text)

            date = html_content.find('span', attrs={'itemprop': 'datePosted'})
            date = date.get_text(' ', strip=True)
            date = datetime.strptime(date, '%d/%m/%Y').strftime('%Y-%m-%d %H:%M:%S')

            #Storing in db

            client = MongoClient(port=27017, username="untaf_dev", password="password", authSource="untaf_dev")
            db = client.untaf_dev

            data = {
                "type": "job",
                "source": "1taf.com",
                "language": "fr",
                "date": date,
                "txt": text,
                "format" : "txt",
                "encoding" : "utf-8",
                "mime" : "text/plain",
                "type d'emplois" : "salarie"
            }
            # Step 3: Insert business object directly into MongoDB via insert_one

            result = db.document.insert_one(data)
            numero.append(offer_addresses[i][-7:])


    with open('numero', 'wb') as yolo:
        pickle.dump(numero, yolo)


    # print(numerodannonceslist)

except urllib.error.URLError:
    print(sys.exc_info()[0],":",sys.exc_info()[1])
    with open("error.log", "a") as tx:
        print(f"{datetime.now():'%Y-%m-%d %H:%M:%S'} source: proxy {sys.exc_info()[0]} : {unidecode.unidecode(str(sys.exc_info()[1]))} Solution: ajouter les lignes de codes suivantes: os.environ['http_proxy']= 'http://172.17.4.161 :8000'"
              f" et 'os.environ['https_proxy']= 'https://172.17.4.161 :8000'", file=tx)
    sys.exit(1)

except urllib.error.HTTPError:
    print(sys.exc_info()[0], ":", sys.exc_info()[1])
    with open("error.log", "a") as tx:
        print(f"{datetime.now()} source: proxy {sys.exc_info()[0]} : {unidecode.unidecode(str(sys.exc_info()[1]))} Solution: liste exhaustive des différents types d'erreur HTTP: https://python.readthedocs.io/en/stable/howto/urllib2.html#httperror", file=tx)
    sys.exit(2)

except:
    print(sys.exc_info()[0], ":", sys.exc_info()[1])
    with open("error.log", "a") as tx:
        print(f"{datetime.now()} source: unknown {sys.exc_info()[0]} : {unidecode.unidecode(str(sys.exc_info()[1]))} Solution: unknown ",file=tx)
    sys.exit(3)













