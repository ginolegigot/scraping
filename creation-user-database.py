from pymongo import MongoClient
import json

#On charge le fichier json
conf = json.load(open("config.json",'r'))

dbusername = conf["user"]
pwdusername = conf["pwd"]
hostname = conf["host"]


#On crée la database
client = MongoClient(port = 27017)

db=client.untaf_dev

db.command( "createUser", dbusername, pwd = pwdusername, roles = [{'role':'readWrite', 'db':'business'}], authenticationRestrictions = [{'clientSource': ["127.0.0.1"], 'serverAddress': [hostname]}] )

